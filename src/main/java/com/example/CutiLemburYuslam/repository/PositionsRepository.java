package com.example.CutiLemburYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.CutiLemburYuslam.model.Positions;

@Repository
public interface PositionsRepository extends JpaRepository<Positions, Integer> {

}
