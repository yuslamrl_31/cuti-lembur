package com.example.CutiLemburYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.CutiLemburYuslam.model.UserLeaveRequest;

@Repository
public interface UserLeaveRequestRepository extends JpaRepository<UserLeaveRequest, Integer> {

}
