package com.example.CutiLemburYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.CutiLemburYuslam.model.BucketApproval;

@Repository
public interface BucketApprovalRepository extends JpaRepository<BucketApproval, Integer> {

}
