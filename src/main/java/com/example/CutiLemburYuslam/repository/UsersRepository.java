package com.example.CutiLemburYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.CutiLemburYuslam.model.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, Integer> {

}
