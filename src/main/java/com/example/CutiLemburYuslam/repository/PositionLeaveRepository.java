package com.example.CutiLemburYuslam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.CutiLemburYuslam.model.PositionLeave;

@Repository
public interface PositionLeaveRepository extends JpaRepository<PositionLeave, Integer> {

}
