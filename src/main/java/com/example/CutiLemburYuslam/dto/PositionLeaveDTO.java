package com.example.CutiLemburYuslam.dto;

import java.util.Date;

public class PositionLeaveDTO {

	private int idPositionLeave;
	private PositionsDTO positions;
	private int jatahCuti;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	public PositionLeaveDTO() {
		
	}

	public PositionLeaveDTO(int idPositionLeave, PositionsDTO positions, int jatahCuti, String createdBy,
			Date createdDate, String updatedBy, Date updatedDate) {
		super();
		this.idPositionLeave = idPositionLeave;
		this.positions = positions;
		this.jatahCuti = jatahCuti;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	public int getIdPositionLeave() {
		return idPositionLeave;
	}

	public void setIdPositionLeave(int idPositionLeave) {
		this.idPositionLeave = idPositionLeave;
	}

	public PositionsDTO getPositions() {
		return positions;
	}

	public void setPositions(PositionsDTO positions) {
		this.positions = positions;
	}

	public int getJatahCuti() {
		return jatahCuti;
	}

	public void setJatahCuti(int jatahCuti) {
		this.jatahCuti = jatahCuti;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	
	
}
