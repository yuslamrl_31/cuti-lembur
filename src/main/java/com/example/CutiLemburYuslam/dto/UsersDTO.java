package com.example.CutiLemburYuslam.dto;

import java.util.Date;

public class UsersDTO {

	private int idUser;
	private PositionsDTO positions;
	private String namaUser;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	public UsersDTO() {
		
	}

	public UsersDTO(int idUser, PositionsDTO positions, String namaUser, String createdBy, Date createdDate,
			String updatedBy, Date updatedDate) {
		super();
		this.idUser = idUser;
		this.positions = positions;
		this.namaUser = namaUser;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public PositionsDTO getPositions() {
		return positions;
	}

	public void setPositions(PositionsDTO positions) {
		this.positions = positions;
	}

	public String getNamaUser() {
		return namaUser;
	}

	public void setNamaUser(String namaUser) {
		this.namaUser = namaUser;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	
	
}
