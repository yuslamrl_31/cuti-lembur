package com.example.CutiLemburYuslam.dto;

import java.util.Date;

public class UserLeaveRequestDTO {

	private int idUserLeaveRequest;
	private UsersDTO users;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private String description;
	private String status;
	private Date datePengajuan;
	private int remainingDaysOff;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	public UserLeaveRequestDTO() {
		
	}

	public UserLeaveRequestDTO(int idUserLeaveRequest, UsersDTO users, Date leaveDateFrom, Date leaveDateTo,
			String description, String status, Date datePengajuan, int remainingDaysOff, String createdBy,
			Date createdDate, String updatedBy, Date updatedDate) {
		super();
		this.idUserLeaveRequest = idUserLeaveRequest;
		this.users = users;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.description = description;
		this.status = status;
		this.datePengajuan = datePengajuan;
		this.remainingDaysOff = remainingDaysOff;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	public int getIdUserLeaveRequest() {
		return idUserLeaveRequest;
	}

	public void setIdUserLeaveRequest(int idUserLeaveRequest) {
		this.idUserLeaveRequest = idUserLeaveRequest;
	}

	public UsersDTO getUsers() {
		return users;
	}

	public void setUsers(UsersDTO users) {
		this.users = users;
	}

	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}

	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}

	public Date getLeaveDateTo() {
		return leaveDateTo;
	}

	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDatePengajuan() {
		return datePengajuan;
	}

	public void setDatePengajuan(Date datePengajuan) {
		this.datePengajuan = datePengajuan;
	}

	public int getRemainingDaysOff() {
		return remainingDaysOff;
	}

	public void setRemainingDaysOff(int remainingDaysOff) {
		this.remainingDaysOff = remainingDaysOff;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	
}
