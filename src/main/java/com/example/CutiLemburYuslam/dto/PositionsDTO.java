package com.example.CutiLemburYuslam.dto;

import java.util.Date;

public class PositionsDTO {

	private int idPosition;
	private String namaPosition;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	public PositionsDTO() {
		
	}

	public PositionsDTO(int idPosition, String namaPosition, String createdBy, Date createdDate, String updatedBy,
			Date updatedDate) {
		super();
		this.idPosition = idPosition;
		this.namaPosition = namaPosition;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	public int getIdPosition() {
		return idPosition;
	}

	public void setIdPosition(int idPosition) {
		this.idPosition = idPosition;
	}

	public String getNamaPosition() {
		return namaPosition;
	}

	public void setNamaPosition(String namaPosition) {
		this.namaPosition = namaPosition;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	
}
