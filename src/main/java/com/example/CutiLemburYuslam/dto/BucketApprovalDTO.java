package com.example.CutiLemburYuslam.dto;

import java.util.Date;

public class BucketApprovalDTO {

	private int idBucketApproval;
	private UserLeaveRequestDTO userLeaveRequest;
	private String status;
	private String resolverReason;
	private String resolvedBy;
	private Date resolvedDate;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	public BucketApprovalDTO() {
		
	}

	public int getIdBucketApproval() {
		return idBucketApproval;
	}

	public void setIdBucketApproval(int idBucketApproval) {
		this.idBucketApproval = idBucketApproval;
	}

	public UserLeaveRequestDTO getUserLeaveRequest() {
		return userLeaveRequest;
	}

	public void setUserLeaveRequest(UserLeaveRequestDTO userLeaveRequest) {
		this.userLeaveRequest = userLeaveRequest;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResolverReason() {
		return resolverReason;
	}

	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}

	public String getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public Date getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	
	
}
