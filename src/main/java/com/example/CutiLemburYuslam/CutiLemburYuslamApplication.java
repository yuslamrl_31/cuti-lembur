package com.example.CutiLemburYuslam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CutiLemburYuslamApplication {

	public static void main(String[] args) {
		SpringApplication.run(CutiLemburYuslamApplication.class, args);
	}

}
