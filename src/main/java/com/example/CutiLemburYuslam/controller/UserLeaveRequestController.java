package com.example.CutiLemburYuslam.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.CutiLemburYuslam.dto.PositionLeaveDTO;
import com.example.CutiLemburYuslam.dto.PositionsDTO;
import com.example.CutiLemburYuslam.dto.UserLeaveRequestDTO;
import com.example.CutiLemburYuslam.model.BucketApproval;
import com.example.CutiLemburYuslam.model.PositionLeave;
import com.example.CutiLemburYuslam.model.Positions;
import com.example.CutiLemburYuslam.model.UserLeaveRequest;
import com.example.CutiLemburYuslam.model.Users;
import com.example.CutiLemburYuslam.repository.BucketApprovalRepository;
import com.example.CutiLemburYuslam.repository.PositionLeaveRepository;
import com.example.CutiLemburYuslam.repository.UserLeaveRequestRepository;
import com.example.CutiLemburYuslam.repository.UsersRepository;

@RestController
@RequestMapping("/api")
public class UserLeaveRequestController {

	@Autowired
	UserLeaveRequestRepository userLeaveRequestRepository;
	
	@Autowired
	UsersRepository usersRepository;
	
	@Autowired
	PositionLeaveRepository positionLeaveRepository;
	
	@Autowired
	BucketApprovalRepository bucketApprovalRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	public UserLeaveRequest convertToEntity(UserLeaveRequestDTO userLeaveRequestDTO) {
		UserLeaveRequest userLeaveRequest = modelMapper.map(userLeaveRequestDTO, UserLeaveRequest.class);
		return userLeaveRequest;		
	}
	
	public UserLeaveRequestDTO convertToDto(UserLeaveRequest userLeaveRequest) {
		UserLeaveRequestDTO userLeaveRequestDTO = modelMapper.map(userLeaveRequest, UserLeaveRequestDTO.class);
		return userLeaveRequestDTO;
	}
	
	//Buat API POST requestLeave, digunakan untuk mengajukan permohonan cuti 
	@PostMapping("/requestLeave")
	public HashMap<String, Object> createUserLeaveRequest(@Valid @RequestBody UserLeaveRequestDTO userLeaveRequestDTO){
		HashMap<String, Object> hmUserLeaveRequest = new HashMap<String, Object>();
		List<PositionLeaveDTO> positionLeaveDTOs = new ArrayList<PositionLeaveDTO>();
		List<UserLeaveRequestDTO> userLeaveRequestDTOs = new ArrayList<UserLeaveRequestDTO>();
		
		String status = "Waiting";
		boolean isExist = false;
		int timeFrom = userLeaveRequestDTO.getLeaveDateFrom().getDate();
		int timeTo = userLeaveRequestDTO.getLeaveDateTo().getDate();
		int timeDifference = timeTo - timeFrom;
		Date today = new Date();
		int timeNow = today.getDate();
		
		BucketApproval bucketApproval = new BucketApproval();
		UserLeaveRequest userLeaveRequest = convertToEntity(userLeaveRequestDTO);
		
		long todayDifference = timeFrom - timeNow;
		
		//if exist
		for(UserLeaveRequest request : userLeaveRequestRepository.findAll()) {
			if(userLeaveRequest.getUsers().getIdUser() == request.getUsers().getIdUser() && userLeaveRequest.getLeaveDateFrom().getYear() == request.getLeaveDateFrom().getYear()) {
				isExist=true;
				userLeaveRequest.setRemainingDaysOff(request.getRemainingDaysOff());
			}
		}
		if(!isExist) {
			for(Users users : usersRepository.findAll()) {
				for(PositionLeave positionLeave : positionLeaveRepository.findAll()) {
				if(positionLeave.getPositions().getIdPosition() == users.getPositions().getIdPosition()) {
					userLeaveRequest.setRemainingDaysOff(positionLeave.getJatahCuti());
				}
				
			}
		}
		}
		
		if(userLeaveRequest.getRemainingDaysOff() == 0) {
			hmUserLeaveRequest.put("Error : ", "Jatah Cuti Habis");
			hmUserLeaveRequest.put("Message : ", "Mohon maaf, jatah cuti anda telah habis");
		}
		else if(timeDifference > userLeaveRequest.getRemainingDaysOff()) {
			hmUserLeaveRequest.put("Error : ", "Jatah Cuti Tidak Cukup");
			hmUserLeaveRequest.put("Message : ", "Mohon maaf, jatah cuti Anda tidak cukup untuk digunakan dari tanggal " + userLeaveRequest.getLeaveDateFrom() + " sampai " + userLeaveRequest.getLeaveDateTo() + "(" + timeDifference + " hari). Jatah cuti Anda yang tersisa adalah " + userLeaveRequest.getRemainingDaysOff());
		}
		else if(timeDifference < 0) {
			hmUserLeaveRequest.put("Error ",  "Tanggal Salah, leaveDateFrom > leaveDateTo");
			hmUserLeaveRequest.put("message : ", "Tanggal yang Anda ajukan tidak valid");
		}
		else if(todayDifference < 0) {
			hmUserLeaveRequest.put("Error : ", "Cuti Backdate, tanggal pengajuan cuti < tanggal hari ini");
			hmUserLeaveRequest.put("Message : ", "Tanggal yang Anda ajukan telah lampau, silahkan ganti tanggal pengajuan cuti anda");
		}
		
		else {
			hmUserLeaveRequest.put("Message : ", "Permohonan sedang di proses.");
			userLeaveRequest.setStatus(status);
			userLeaveRequest.setDatePengajuan(today);
			bucketApproval.setUserLeaveRequest(userLeaveRequest);
			userLeaveRequestRepository.save(userLeaveRequest);
			UserLeaveRequest leaveRequest = userLeaveRequestRepository.findById(userLeaveRequest.getIdUserLeaveRequest()).get();
			bucketApproval.setUserLeaveRequest(leaveRequest);
			bucketApproval.setStatus(leaveRequest.getStatus());
			bucketApproval.setIdBucketApproval(leaveRequest.getIdUserLeaveRequest());
			bucketApprovalRepository.save(bucketApproval);
		}
		return hmUserLeaveRequest;		
		
	}
	
	
	//Tambahkan 1 Fitur API sesuai dengan kreativitas Anda sesuai dengan kasus yang telah diberikan
	@GetMapping("/user_leave_request/read")
	public HashMap<String, Object>readAllUserLeaveRequest(){
		HashMap<String, Object> hmUserLeaveRequest = new HashMap<String, Object>();
		List<UserLeaveRequestDTO> userLeaveRequestDTOs = new ArrayList<UserLeaveRequestDTO>();
		for (UserLeaveRequest userLeaveRequest : userLeaveRequestRepository.findAll()) {
			userLeaveRequestDTOs.add(convertToDto(userLeaveRequest));
		}
		
		hmUserLeaveRequest.put("Message : ", "Read User Leave Request Succes!");
		hmUserLeaveRequest.put("Total : ", userLeaveRequestDTOs.size());
		hmUserLeaveRequest.put("Data : ", userLeaveRequestDTOs);
		
		return hmUserLeaveRequest;
	}
	
	
	
}
