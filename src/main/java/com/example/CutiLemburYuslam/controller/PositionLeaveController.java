package com.example.CutiLemburYuslam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.CutiLemburYuslam.dto.PositionLeaveDTO;
import com.example.CutiLemburYuslam.model.PositionLeave;
import com.example.CutiLemburYuslam.repository.PositionLeaveRepository;

@RestController
@RequestMapping("/api")
public class PositionLeaveController {

	@Autowired
	PositionLeaveRepository positionLeaveRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	public PositionLeave convertToEntity(PositionLeaveDTO positionLeaveDTO) {
		PositionLeave positionLeave = modelMapper.map(positionLeaveDTO, PositionLeave.class);
		return positionLeave;		
	}
	
	public PositionLeaveDTO convertToDto(PositionLeave positionLeave) {
		PositionLeaveDTO positionLeaveDTO = modelMapper.map(positionLeave, PositionLeaveDTO.class);
		return positionLeaveDTO;
	}
	
	//Read All Position Leave
	@GetMapping("/position_leave/all")
	public HashMap<String, Object>readAllPositionLeave(){
		HashMap<String, Object> hmPositionLeave = new HashMap<String, Object>();
		List<PositionLeaveDTO> positionLeaveDTOs = new ArrayList<PositionLeaveDTO>();
		for (PositionLeave positionLeave : positionLeaveRepository.findAll()) {
			positionLeaveDTOs.add(convertToDto(positionLeave));
		}
		
		hmPositionLeave.put("Message : ", "Read All Position Leave Succes!");
		hmPositionLeave.put("Total : ", positionLeaveDTOs.size());
		hmPositionLeave.put("Data : ", positionLeaveDTOs);
		
		return hmPositionLeave;
	}
	
	//Read Position Leave By ID
	@GetMapping("/position_leave/{id}")
	public HashMap<String, Object> readPositionLeaveById(@PathVariable(name="id") Integer id){
		HashMap<String, Object> hmPositionLeave = new HashMap<String, Object>();
		PositionLeave positionLeave = positionLeaveRepository.findById(id).orElseThrow(null);
		PositionLeaveDTO positionLeaveDTO = convertToDto(positionLeave);
		
		hmPositionLeave.put("Message : ", "Read Position Leave By ID succes!");
		hmPositionLeave.put("Data : ", positionLeaveDTO);
		
		return hmPositionLeave;
	}
	
	//Create Position Leave
	@PostMapping("/position_leave/add")
	public HashMap<String, Object> createPositionLeave(@Valid @RequestBody PositionLeaveDTO positionLeaveDTO){
		HashMap<String, Object> hmPositionLeave = new HashMap<String, Object>();
		PositionLeave positionLeave = convertToEntity(positionLeaveDTO);
		positionLeaveRepository.save(positionLeave);
		
		hmPositionLeave.put("Message : ", "Create Position Leave Succes!");
		hmPositionLeave.put("Data : ", positionLeave);
		
		return hmPositionLeave;		
		
	}
	
	
	//Update Position Leave
	@PutMapping("position_leave/update/{id}")
	public HashMap<String, Object> updatePositionLeave(@PathVariable(value="id") Integer id, @Valid @RequestBody PositionLeaveDTO positionLeaveDTO){
		HashMap<String, Object> hmPositionLeave = new HashMap<String, Object>();
		PositionLeave positionLeave = positionLeaveRepository.findById(id).orElseThrow(null);
			
		positionLeaveDTO.setIdPositionLeave(positionLeave.getIdPositionLeave());
			
		if(positionLeaveDTO.getPositions() != null) {
			positionLeave.setPositions(convertToEntity(positionLeaveDTO).getPositions());
		}
		
		if(positionLeaveDTO.getJatahCuti() != 0) {
			positionLeave.setJatahCuti(convertToEntity(positionLeaveDTO).getJatahCuti());
		}
		
		positionLeaveRepository.save(positionLeave);
		
		hmPositionLeave.put("Message : ", "Update Position Leave Succes!");
		hmPositionLeave.put("Data : ", positionLeave);
		
		return hmPositionLeave;
	}
	
	//Delete Position Leave
	@DeleteMapping("position_leave/delete/{id}")
	public HashMap<String, Object> deletePositionLeave(@PathVariable (name="id") Integer id){
		HashMap<String, Object> hmPositionLeave = new HashMap<String, Object>();
		PositionLeave positionLeave = positionLeaveRepository.findById(id).orElseThrow(null);
		
		positionLeaveRepository.delete(positionLeave);
		
		hmPositionLeave.put("Message : ", "Delete Position Leave Succes!");
		hmPositionLeave.put("Data : ", positionLeave);
		
		return hmPositionLeave;
	}
	
}
