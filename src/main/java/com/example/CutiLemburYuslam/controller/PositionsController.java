package com.example.CutiLemburYuslam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.CutiLemburYuslam.dto.PositionsDTO;
import com.example.CutiLemburYuslam.model.Positions;
import com.example.CutiLemburYuslam.repository.PositionsRepository;

@RestController
@RequestMapping("/api")
public class PositionsController {

	@Autowired
	PositionsRepository positionsRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	public Positions convertToEntity(PositionsDTO positionsDTO) {
		Positions positions = modelMapper.map(positionsDTO, Positions.class);
		return positions;		
	}
	
	public PositionsDTO convertToDto(Positions positions) {
		PositionsDTO positionsDTO = modelMapper.map(positions, PositionsDTO.class);
		return positionsDTO;
	}
	
	//Read All Position
	@GetMapping("/positions/all")
	public HashMap<String, Object>readAllPositions(){
		HashMap<String, Object> hmPositions = new HashMap<String, Object>();
		List<PositionsDTO> positionsDTOs = new ArrayList<PositionsDTO>();
		for (Positions positions : positionsRepository.findAll()) {
			positionsDTOs.add(convertToDto(positions));
		}
		
		hmPositions.put("Message : ", "Read All Positions Succes!");
		hmPositions.put("Total : ", positionsDTOs.size());
		hmPositions.put("Data : ", positionsDTOs);
		
		return hmPositions;
	}
	
	//Read Position By ID
	@GetMapping("/positions/{id}")
	public HashMap<String, Object> readPositionsById(@PathVariable(name="id") Integer id){
		HashMap<String, Object> hmPositions = new HashMap<String, Object>();
		Positions positions = positionsRepository.findById(id).orElseThrow(null);
		PositionsDTO positionsDTO = convertToDto(positions);
		
		hmPositions.put("Message : ", "Read Positions By ID succes!");
		hmPositions.put("Data : ", positionsDTO);
		
		return hmPositions;
	}
	
	//Create Positions
	@PostMapping("/positions/add")
	public HashMap<String, Object> createPositions(@Valid @RequestBody PositionsDTO positionsDTO){
		HashMap<String, Object> hmPosition = new HashMap<String, Object>();
		Positions positions = convertToEntity(positionsDTO);
		positionsRepository.save(positions);
		
		hmPosition.put("Message : ", "Create Positions Succes!");
		hmPosition.put("Data : ", positions);
		
		return hmPosition;		
		
	}
	
	//Update Positions
	@PutMapping("positions/update/{id}")
	public HashMap<String, Object> updatePositions(@PathVariable(value="id") Integer id, @Valid @RequestBody PositionsDTO positionsDTO){
		HashMap<String, Object> hmPositions = new HashMap<String, Object>();
		Positions positions = positionsRepository.findById(id).orElseThrow(null);
			
		positionsDTO.setIdPosition(positions.getIdPosition());
			
		if(positionsDTO.getNamaPosition() != null) {
			positions.setNamaPosition(convertToEntity(positionsDTO).getNamaPosition());
		}
		
		positionsRepository.save(positions);
		
		hmPositions.put("Message : ", "Update Positions Succes!");
		hmPositions.put("Data : ", positions);
		
		return hmPositions;
	}
	
	//Delete Positions
	@DeleteMapping("positions/delete/{id}")
	public HashMap<String, Object> deletePositions(@PathVariable (name="id") Integer id){
		HashMap<String, Object> hmPositions = new HashMap<String, Object>();
		Positions positions = positionsRepository.findById(id).orElseThrow(null);
		
		positionsRepository.delete(positions);
		
		hmPositions.put("Message : ", "Delete Position Succes!");
		hmPositions.put("Data : ", positions);
		
		return hmPositions;
	}
}
