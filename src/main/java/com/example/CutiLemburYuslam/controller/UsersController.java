package com.example.CutiLemburYuslam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.CutiLemburYuslam.dto.UsersDTO;
import com.example.CutiLemburYuslam.model.Users;
import com.example.CutiLemburYuslam.repository.UsersRepository;

@RestController
@RequestMapping("/api")
public class UsersController {

	@Autowired
	UsersRepository usersRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	public Users convertToEntity(UsersDTO usersDTO) {
		Users users = modelMapper.map(usersDTO, Users.class);
		return users;		
	}
	
	public UsersDTO convertToDto(Users users) {
		UsersDTO usersDTO = modelMapper.map(users, UsersDTO.class);
		return usersDTO;
	}
	
	//Read All Users
	@GetMapping("/users/all")
	public HashMap<String, Object>readAllUsers(){
		HashMap<String, Object> hmUsers = new HashMap<String, Object>();
		List<UsersDTO> usersDTOs = new ArrayList<UsersDTO>();
		for (Users users : usersRepository.findAll()) {
			usersDTOs.add(convertToDto(users));
		}
		
		hmUsers.put("Message : ", "Read All Users Succes!");
		hmUsers.put("Total : ", usersDTOs.size());
		hmUsers.put("Data : ", usersDTOs);
		
		return hmUsers;
	}
	
	//Read Users By ID
	@GetMapping("/users/{id}")
	public HashMap<String, Object> readUsersById(@PathVariable(name="id") Integer id){
		HashMap<String, Object> hmUsers = new HashMap<String, Object>();
		Users users = usersRepository.findById(id).orElseThrow(null);
		UsersDTO usersDTO = convertToDto(users);
		
		hmUsers.put("Message : ", "Read Users By ID succes!");
		hmUsers.put("Data : ", usersDTO);
		
		return hmUsers;
	}
	
	//Create Users
	@PostMapping("/users/add")
	public HashMap<String, Object> createUsers(@Valid @RequestBody UsersDTO usersDTO){
		HashMap<String, Object> hmUsers = new HashMap<String, Object>();
		Users users = convertToEntity(usersDTO);
		usersRepository.save(users);
		
		hmUsers.put("Message : ", "Create Users Succes!");
		hmUsers.put("Data : ", users);
		
		return hmUsers;		
		
	}
	
	//Update Users
	@PutMapping("users/update/{id}")
	public HashMap<String, Object> updateUsers(@PathVariable(value="id") Integer id, @Valid @RequestBody UsersDTO usersDTO){
		HashMap<String, Object> hmUsers = new HashMap<String, Object>();
		Users users = usersRepository.findById(id).orElseThrow(null);
			
		usersDTO.setIdUser(users.getIdUser());	
		
		if(usersDTO.getPositions() != null ) {
			users.setPositions(convertToEntity(usersDTO).getPositions());
		}
		
		if(usersDTO.getNamaUser() != null) {
			users.setNamaUser(convertToEntity(usersDTO).getNamaUser());
		}
			
		usersRepository.save(users);
		
		hmUsers.put("Message : ", "Update Users Succes!");
		hmUsers.put("Data : ", users);
		
		return hmUsers;
	}
	
	//Delete Users 
	@DeleteMapping("users/delete/{id}")
	public HashMap<String, Object> deleteUsers(@PathVariable (name="id") Integer id){
		HashMap<String, Object> hmUsers = new HashMap<String, Object>();
		Users users = usersRepository.findById(id).orElseThrow(null);
		
		usersRepository.delete(users);
		
		hmUsers.put("Message : ", "Delete Users Succes!");
		hmUsers.put("Data : ", users);
		
		return hmUsers;
	}
	
	
}
